#!/bin/bash

kubectl apply -f deploy/cr.yaml
kubectl create sa tiller -n kube-system
kubectl apply -f deploy/crb.yaml
kubectl edit deployment -n kube-system tiller-deploy
