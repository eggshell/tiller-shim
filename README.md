# tiller-shim

When installing tiller into a kubernetes cluster, it is possible for the tiller
pod to not have enough permissions to create resources in RBAC-enabled clusters.
This is a workaround to give the tiller pod pretty loose permissions to do what
it wants in the cluster it is deployed in.

I would not recommend using this in production for security reasons, but this
will help initial development get off the ground without too much frustration.

## Usage

This will create a ClusterRole, ServiceAccount, and ClusterRoleBinding for
tiller.

```bash
./tiller-shim.sh
```

The final `kubectl edit deployment` step should pop you into your default
editor. Make sure to set `serviceAccountName` to `tiller` in the pod spec for
this deployment.
